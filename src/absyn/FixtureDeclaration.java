package absyn;

import java.io.FileWriter;
import java.io.IOException;

import semantical.TypeChecker;
import types.ClassType;
import types.FixtureSignature;
import types.VoidType;

/**
 * A node of abstract syntax representing the declaration of a fixture of a
 * Kitten class.
 *
 * @author <A HREF="mailto:fausto.spoto@univr.it">Fausto Spoto</A>
 */

public class FixtureDeclaration extends CodeDeclaration {

	public FixtureDeclaration(int pos, Command body, ClassMemberDeclaration next) {
		super(pos, null, body, next);
	}

	/**
	 * Yields the signature of this method declaration.
	 *
	 * @return the signature of this method declaration. Yields {@code null} if
	 *         type-checking has not been performed yet
	 */
	@Override
	public FixtureSignature getSignature() {
		return (FixtureSignature) super.getSignature();
	}

	/**
	 * Adds arcs between the dot node for this piece of abstract syntax and
	 * those representing [@link {@link #body}.
	 *
	 * @param where
	 *            the file where the dot representation must be written
	 */
	@Override
	protected void toDotAux(FileWriter where) throws IOException {
		linkToNode("body", getBody().toDot(where), where);
	}

	@Override
	protected void addTo(ClassType clazz) {
		FixtureSignature fSig = new FixtureSignature(clazz, this);

		clazz.addFixture(fSig);

		// we record the signature of this test inside this abstract syntax
		setSignature(fSig);
	}

	/**
	 * Type-checks this fixture declaration. It first checks that if this
	 * fixture overrides a fixture of a superclass. Then it builds a
	 * type-checker whose only variable in scope is {@code this} of type
	 * {@code clazz}. It then type-checks the body of the fixture in that
	 * type-checker.
	 *
	 * @param clazz
	 *            the semantical type of the class where this method occurs
	 */

	@Override
	protected void typeCheckAux(ClassType clazz) {
		TypeChecker checker = new TypeChecker(VoidType.INSTANCE,
				clazz.getErrorMsg());
		checker = checker.putVar("this", clazz);

		// we type-check the body of the fixture in the resulting
		// type-checker
		getBody().typeCheck(checker);

		// we check that there is no dead-code in the body of the fixture
		getBody().checkForDeadcode();
	}
}