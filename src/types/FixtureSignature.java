package types;

import javaBytecodeGenerator.TestClassGenerator;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import translation.Block;
import absyn.FixtureDeclaration;

public class FixtureSignature extends CodeSignature {

	// yield the number of the fixtures already constructed
	private static int counter = 0;

	/**
	 * Constructs the signature of a fixture with a sequential name
	 * 
	 * @param clazz
	 *            the class where this method is defined
	 * @param abstractSyntax
	 *            the abstract syntax of the declaration of this fixture
	 */

	public FixtureSignature(ClassType clazz, FixtureDeclaration abstractSyntax) {
		super(clazz, VoidType.INSTANCE, TypeList.EMPTY, "fixture" + counter++,
				abstractSyntax);
	}

	/**
	 * Adds to the given class generator a Java bytecode method for this
	 * fixture.
	 *
	 * @param classGen
	 *            the generator of the class where the fixture lives
	 */

	public void createFixture(TestClassGenerator classGen) {
		InstructionList il = new InstructionList();

		il.append(getCode().getBytecode().generateJavaBytecode(classGen));

		MethodGen methodGen = new MethodGen(Constants.ACC_PRIVATE
				| Constants.ACC_STATIC, // private static
				Type.VOID, // return type
				new Type[] { (getDefiningClass()).toBCEL() }, // parameters
																// types, if
				// any
				null, // parameters names: we do not care
				getName(), // method's name
				classGen.getClassName(), // defining class
				il, // bytecode of the method
				classGen.getConstantPool()); // constant pool

		// we must always call these methods before the getMethod()
		// method below. They set the number of local variables and stack
		// elements used by the code of the method
		methodGen.setMaxStack();
		methodGen.setMaxLocals();

		// we add a method to the class that we are generating
		classGen.addMethod(methodGen.getMethod());
	}

	@Override
	protected Block addPrefixToCode(Block code) {
		return code;
	}

}
