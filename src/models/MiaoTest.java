package models;

import java.util.concurrent.TimeUnit;

public class MiaoTest extends java.lang.Object {

	private static void fixture0(Miao m) {
		m.i = 10;
	}

	private static void fixture1(Miao m) {
		m.i = 20;
	}

	private static void fixture2(Miao m) {
		m.i = 30;
	}

	private static String test1() {
		Miao m = new Miao();
		fixture0(m);
		fixture1(m);
		fixture2(m);
		m.i = 30;
		if (m.i > 10) // assertCondition
			if (m.i > 20) { // assertCondition
				System.out.println("ciccio");
				return null;
			} else
				return new runTime.String("7::5").toString();
		else
			return new runTime.String("6::5").toString();
	}

	private static String test2() {
		Miao m = new Miao();
		fixture0(m);
		fixture1(m);
		fixture2(m);
		m.i = 21;
		if (m.i > 20) { // assertCondition
			System.out.println("pluto");
			return null;
		} else
			return new runTime.String("7::5").toString();
	}

	public static void main(String args[]) {
		int passedTests = 0, failedTest = 0;
		System.out.println("Executing tests..");
		long testsTimeStart = System.nanoTime();
		StringBuilder outBuilder = new StringBuilder("\nResults:");
		String lastTestResult;

		long testTimeStart = System.nanoTime();
		outBuilder.append("\n\t- test1: ");
		lastTestResult = test1();
		if (lastTestResult == null) {
			outBuilder.append("passed [");
			passedTests++;
		} else {
			outBuilder.append("failed [");
			failedTest++;
		}
		outBuilder
				.append(TimeUnit.NANOSECONDS.toMicros(System.nanoTime()
						- testTimeStart) / 1000.000).append("ms] ")
				.append(lastTestResult != null ? lastTestResult : "");

		testTimeStart = System.nanoTime();
		outBuilder.append("\n\t- test2: ");
		lastTestResult = test2();
		if (lastTestResult == null) {
			outBuilder.append("passed [");
			passedTests++;
		} else {
			outBuilder.append("failed [");
			failedTest++;
		}
		outBuilder
				.append(TimeUnit.NANOSECONDS.toMicros(System.nanoTime()
						- testTimeStart) / 1000.000).append("ms] ")
				.append(lastTestResult != null ? lastTestResult : "");

		System.out
				.println(outBuilder
						.append("\n\n")
						.append(passedTests)
						.append(" tests passed, ")
						.append(failedTest)
						.append(" failed [")
						.append(TimeUnit.NANOSECONDS.toMicros(System.nanoTime()
								- testsTimeStart) / 1000.000).append("ms] ")
						.toString());

	}
}