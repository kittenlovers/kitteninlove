package syntactical;

import java.io.FileWriter;
import java.io.IOException;

import java_cup.runtime.Symbol;
import lexical.Lexer;
import absyn.ClassDefinition;

public class Main {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		if (args.length == 0)
			System.out
					.println("You must specify a Kitten class name to compile");
		else
			try {
				String fileName = args[0];
				Parser parser = new Parser(new Lexer(fileName));
				Symbol symbol = parser.parse();
				System.out.println("End of the syntactical analysis");

				ClassDefinition absyn = (ClassDefinition) symbol.value;
				if (absyn != null) {
					String dotName = fileName.substring(0, fileName.length()
							- ".kit".length())
							+ ".dot";

					String pdfName = fileName.substring(0, fileName.length()
							- ".kit".length())
							+ ".pdf";
					try (FileWriter file = new FileWriter(dotName)) {
						absyn.toDot(file);
					}
					System.out.println("Abstract syntax saved into " + dotName);

					// mod to PDF
					if (false) { // if set to TRUE generate a PDF from the dot
									// file
						GraphViz gv = new GraphViz();
						gv.readSource(dotName);
						gv.writeGraphToFile(
								gv.getGraph(gv.getDotSource(), "pdf"), pdfName);
						System.out.println("Abstract syntax tree saved into "
								+ pdfName);
					}
					// END mod to PDF

				} else
					System.out.println("Null semantical value");
			} catch (IOException e) {
				System.out.println("I/O error");
			} catch (Error e) {
				System.out.println("Unmatched input");
			}
	}
}