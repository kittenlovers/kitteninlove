package javaBytecodeGenerator;

import java.io.PrintStream;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.BranchHandle;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DDIV;
import org.apache.bcel.generic.GOTO;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IFNULL;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2D;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC2_W;
import org.apache.bcel.generic.LLOAD;
import org.apache.bcel.generic.LSUB;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import types.ClassMemberSignature;
import types.ClassType;
import types.FixtureSignature;
import types.TestSignature;

/**
 * @author kittenlovers
 *
 */
@SuppressWarnings("serial")
public class TestClassGenerator extends ClassGenerator {

	/**
	 * Builds a test class generator for the given class type.
	 *
	 * @param clazz
	 *            the class type
	 * @param sigs
	 *            a set of class member signatures. These are those that must be
	 *            translated. If this is {@code null}, all class members are
	 *            translated
	 */

	public TestClassGenerator(ClassType clazz, Set<ClassMemberSignature> sigs) {
		super(clazz.getName() + "Test", // classname
				// the superclass of the Kitten Object class is set to be the
				// Java java.lang.Object class
				clazz.getSuperclass() != null ? clazz.getSuperclass().getName()
						: "java.lang.Object", Constants.ACC_PUBLIC);

		// we add the fixtures
		for (FixtureSignature fixture : clazz.getFixtures())
			if (sigs == null || sigs.contains(fixture))
				fixture.createFixture(this);

		// we add the tests
		for (TestSignature test : clazz.getTests())
			if (sigs == null || sigs.contains(test))
				test.createTest(this, clazz.getFixtures());

		InstructionList il = new InstructionList();

		Type sBType = Type.getType(StringBuilder.class);
		String sBName = StringBuilder.class.getName();
		Type[] arg1Str = { Type.STRING };

		String arg_name[] = { "args" };
		MethodGen methodMain = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_STATIC, Type.VOID, new Type[] { new ArrayType(
				"java.lang.String", 1) }, arg_name, "main", getClassName(), il,
				getConstantPool());

		ConstantPoolGen cpg = methodMain.getConstantPool();
		short ikvt = Constants.INVOKEVIRTUAL;

		il.append(InstructionFactory.ICONST_0);
		il.append(InstructionFactory.ISTORE_1);
		il.append(InstructionFactory.ICONST_0);
		il.append(InstructionFactory.ISTORE_2);

		il.append(getFactory().createPrintln("\n\nExecuting tests.."));
		il.append(getFactory().createInvoke(System.class.getName(), "nanoTime",
				Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
		il.append(InstructionFactory.createStore(Type.LONG, 3));

		il.append(getFactory().createNew(sBName));
		il.append(InstructionConstants.DUP);
		il.append(new LDC(cpg.addString("\nResults: ")));
		il.append(getFactory().createInvoke(sBName, Constants.CONSTRUCTOR_NAME,
				Type.VOID, arg1Str, Constants.INVOKESPECIAL));
		il.append(InstructionFactory.createStore(sBType, 9));

		for (TestSignature testSignature : clazz.getTests()) {

			// salvo nanoTime
			il.append(getFactory()
					.createInvoke(System.class.getName(), "nanoTime",
							Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
			il.append(InstructionFactory.createStore(Type.LONG, 6));

			// load dello string builder e aggiunta del nome del test
			il.append(InstructionFactory.createLoad(sBType, 9));
			il.append(new LDC(cpg.addString("\n\t- " + testSignature.getName()
					+ ": ")));
			il.append(getFactory().createInvoke(sBName, "append", sBType,
					arg1Str, ikvt));

			// chiamata del metodo statico corrispondente al test corrente
			il.append(getFactory().createInvoke(getClassName(),
					testSignature.getName(), Type.STRING, Type.NO_ARGS,
					Constants.INVOKESTATIC));
			il.append(InstructionFactory.createStore(Type.STRING, 5));
			il.append(InstructionFactory.createLoad(Type.STRING, 5));

			// incremento dei contatori
			BranchHandle ifHandle = il.append(new IFNONNULL(null));

			// assert passato
			il.append(InstructionFactory.createLoad(sBType, 9));
			il.append(new LDC(cpg.addString("passed [")));
			il.append(getFactory().createInvoke(sBName, "append", sBType,
					arg1Str, ikvt));
			il.append(InstructionConstants.POP);
			il.append(new IINC(1, 1));

			BranchHandle gotoHandle = il.append(new GOTO(null));

			// assert fallito
			InstructionHandle matchHandle = il.append(InstructionFactory
					.createLoad(sBType, 9));
			il.append(new LDC(cpg.addString("failed [")));
			il.append(getFactory().createInvoke(sBName, "append", sBType,
					arg1Str, ikvt));
			il.append(InstructionConstants.POP);
			il.append(new IINC(2, 1));

			ifHandle.setTarget(matchHandle);

			// stampa dei tempi di esecuzione del test corrente

			InstructionHandle instructionHandle = il.append(InstructionFactory
					.createLoad(sBType, 9));
			il.append(getFactory().createGetStatic(TimeUnit.class.getName(),
					"NANOSECONDS", Type.getType(TimeUnit.class)));

			gotoHandle.setTarget(instructionHandle);

			il.append(getFactory()
					.createInvoke(System.class.getName(), "nanoTime",
							Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
			// carico nanoTimeStart del test corrente
			il.append(new LLOAD(6));
			il.append(new LSUB());
			// converto la differenza tra il tempo iniziale e quella finale in
			// Micros e poi divido per ottenere la precisione decimale
			// desiderata
			il.append(getFactory().createInvoke(TimeUnit.class.getName(),
					"toMicros", Type.LONG, new Type[] { Type.LONG }, ikvt));
			il.append(new L2D());
			int index = cpg.addDouble(1000);
			il.append(new LDC2_W(index));
			il.append(new DDIV());

			// aggiungo il tempo e il risultato del test allo string builder
			il.append(getFactory().createInvoke(sBName, "append", sBType,
					new Type[] { Type.DOUBLE }, ikvt));
			il.append(new LDC(cpg.addString("ms]")));
			il.append(getFactory().createInvoke(sBName, "append", sBType,
					arg1Str, ikvt));

			il.append(InstructionFactory.createLoad(Type.STRING, 5));

			BranchHandle resultNull = il.append(new IFNULL(null));
			il.append(InstructionFactory.createLoad(Type.STRING, 5));
			BranchHandle gotoAppend = il.append(new GOTO(null));

			InstructionHandle ldc = il.append(new LDC(cpg.addString("")));

			InstructionHandle append = il.append(getFactory().createInvoke(
					sBName, "append", sBType, arg1Str, ikvt));

			resultNull.setTarget(ldc);
			gotoAppend.setTarget(append);

		}

		il.append(getFactory().createGetStatic(System.class.getName(), "out",
				Type.getType(PrintStream.class)));

		// stampa del tempo di esecuzione totale di tutti i test
		il.append(InstructionFactory.createLoad(sBType, 9));

		il.append(new LDC(cpg.addString("\n\n")));
		il.append(getFactory().createInvoke(sBName, "append", sBType, arg1Str,
				ikvt));

		il.append(InstructionFactory.ILOAD_1);
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.INT }, ikvt));

		il.append(new LDC(cpg.addString(" tests passed, ")));
		il.append(getFactory().createInvoke(sBName, "append", sBType, arg1Str,
				ikvt));

		il.append(InstructionFactory.ILOAD_2);
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.INT }, ikvt));

		il.append(new LDC(cpg.addString(" failed [")));
		il.append(getFactory().createInvoke(sBName, "append", sBType, arg1Str,
				ikvt));

		il.append(getFactory().createGetStatic(TimeUnit.class.getName(),
				"NANOSECONDS", Type.getType(TimeUnit.class)));

		il.append(getFactory().createInvoke(System.class.getName(), "nanoTime",
				Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
		il.append(new LLOAD(3));
		il.append(new LSUB());

		il.append(getFactory().createInvoke(TimeUnit.class.getName(),
				"toMicros", Type.LONG, new Type[] { Type.LONG }, ikvt));

		il.append(new L2D());
		int index = cpg.addDouble(1000);
		il.append(new LDC2_W(index));
		il.append(new DDIV());

		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.DOUBLE }, ikvt));

		il.append(new LDC(cpg.addString("ms]")));
		il.append(getFactory().createInvoke(sBName, "append", sBType, arg1Str,
				ikvt));

		il.append(getFactory().createInvoke(sBName, "toString", Type.STRING,

		Type.NO_ARGS, ikvt));
		il.append(getFactory().createInvoke(PrintStream.class.getName(),
				"println", Type.VOID,

				arg1Str, ikvt));

		il.append(InstructionConstants.RETURN);

		methodMain.setMaxStack();
		methodMain.setMaxLocals();

		// we add a method to the class that we are generating
		addMethod(methodMain.getMethod());

	}
}
