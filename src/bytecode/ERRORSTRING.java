package bytecode;

import javaBytecodeGenerator.ClassGenerator;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.Type;

/**
 * A bytecode that creates an error string and pushes a reference to it on top
 * of the stack. <br>
 * <br>
 * ... -&gt; ..., new string
 *
 * @author <A HREF="mailto:fausto.spoto@univr.it">Fausto Spoto</A>
 */

public class ERRORSTRING extends NEWSTRING {

	/**
	 * Constructs a bytecode that creates an error string and pushes a reference
	 * to it un the stack.
	 *
	 * @param value
	 *            the lexical value of string
	 */

	public ERRORSTRING(String value) {
		super(value);
	}

	/**
	 * Generates the Java bytecode corresponding to this Kitten bytecode. Kitten
	 * strings are emulated through {@code runTime.String} wrappers of Java
	 * bytecode strings. This way, all methods over Kitten strings can be
	 * emulated through Java methods inside that class (see there). Namely, this
	 * method generates the Java bytecode<br>
	 * <br>
	 * {@code new runTime.String}<br>
	 * {@code dup}<br>
	 * {@code ldc value}<br>
	 * {@code invokespecial runTime.String.&lt;init&gt;}<br>
	 * {@code invokevirtual runTime.String.&lt;toString&gt;}<br>
	 * <br>
	 * that creates a {@code runTime.String} objects and initialises it with the
	 * lexical value {@link #value} of the Kitten string we want to create then
	 * translate it to java.lang.String.
	 *
	 * @param classGen
	 *            the Java class generator to be used for this generation
	 * @return a Java bytecode that creates a {@code java.lang.String} object
	 *         initialised with the lexical {@link #value} of the Kitten string
	 *         that we want to create.
	 */

	@Override
	public InstructionList generateJavaBytecode(ClassGenerator classGen) {
		InstructionFactory factory = classGen.getFactory();
		InstructionList il = super.generateJavaBytecode(classGen);
		String kittenStringName = runTime.String.class.getName();

		// we add a call to the 'toString' method of
		// runTime.String class
		il.append(factory.createInvoke(kittenStringName, "toString",
				Type.STRING, Type.NO_ARGS, Constants.INVOKEVIRTUAL));

		return il;
	}
}